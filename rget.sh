#!/bin/bash

root=/home/bohendo/d/reddget

# node $root/reddget.js $1

if [[ "$1" != "prep" ]]
then
  n=`ls -1 $root/i | sort -r | head -n1 | cut -c 1-2`
  m=`ls -1 $root/i/$n | sort -r | head -n1 | cut -c 1-2`
  more $root/i/$n/$m/* | fold -s -w 140 | less
fi
